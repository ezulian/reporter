"""Initialize the message queue."""
import os
from pathlib import Path

from cki_lib import misc
from cki_lib.logger import get_logger
from datawarehouse import Datawarehouse
from jinja2 import Environment
from jinja2 import FileSystemLoader
from jinja2 import StrictUndefined

from . import utils

DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL', 'http://localhost')
DATAWAREHOUSE_TOKEN = os.environ.get('DATAWAREHOUSE_TOKEN_REPORTER')

LOGGER = get_logger('cki.reporter')

DATAWAREHOUSE = Datawarehouse(DATAWAREHOUSE_URL, token=DATAWAREHOUSE_TOKEN)

TEMPLATE_DIR = Path(__file__).resolve().parent / 'templates'

TEMPLATES = {'short': 'short.j2',
             'long': 'long.j2',
             'failure-only': 'failure-only.j2'}

# When any of these templates are selected, checkouts that passed will be ignored when email sending
FAILURE_ONLY_TEMPLATES = ['failure-only']

# The default template
SELECTED_TEMPLATE = 'short'

SMTP_URL = os.environ.get("SMTP_URL", 'localhost')
SMTP_DEBUG = misc.get_env_bool("SMTP_DEBUG", False)
REPORTER_EMAIL_FROM = os.environ.get("REPORTER_EMAIL_FROM")
BCC_ARCHIVE_ADDRESS = os.environ.get("BCC_ARCHIVE_ADDRESS")

EMAILING_ENABLED = misc.get_env_bool("REPORTER_EMAILING_ENABLED")

# Set up the jinja2 environment which can be reused throughout this script.
JINJA_ENV = Environment(
    loader=FileSystemLoader(TEMPLATE_DIR),
    trim_blocks=True,  # Remove first newline after a jinja2 block
    lstrip_blocks=True,  # Strip whitespace from the left side of tags
    undefined=StrictUndefined,
)

JINJA_ENV.filters['status_to_emoji'] = utils.status_to_emoji
JINJA_ENV.filters['yesno'] = utils.yesno
JINJA_ENV.filters['unique_test_descriptions'] = utils.unique_test_descriptions
