"""Main reporter definition."""
from argparse import ArgumentParser
from functools import partial
import os
import sys

from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
import sentry_sdk

from . import settings
from .data import CheckoutData
from .emailer import render_template
from .emailer import send_emails


def print_report(checkout_data):
    """Render the checkout's report and print it."""
    print(render_template(checkout_data))


def process_message(body=None, func=None, **_):
    """Process the webhook message."""
    object_type = body['object_type']
    object_id = misc.get_nested_key(body, 'object/id')

    settings.LOGGER.info('Processing message for %s %s', object_type, object_id)

    if object_type != 'checkout':
        settings.LOGGER.debug('  ignoring, unsupported object type: %s', object_type)
        return

    if misc.get_nested_key(body, 'object/misc/retrigger', False):
        settings.LOGGER.debug('  ignoring, retriggered checkout')
        return

    # If this checkout is related to a merge request, we don't need to report anything
    if misc.get_nested_key(body, 'object/misc/related_merge_request'):
        settings.LOGGER.debug('  ignoring, checkout associated with a merge request')
        return

    if (status := body['status']) != 'ready_to_report':
        settings.LOGGER.debug('  ignoring, unsupported message status: %s', status)
        return

    func(CheckoutData(object_id))


def create_parser():
    """Create argument parser."""
    parser = ArgumentParser('Create reports for provided pipelines')
    parser.add_argument('-t', '--template', type=str,
                        metavar='TEMPLATE_NAME',
                        choices=list(settings.TEMPLATES.keys()),
                        help='Template name to be used. When email reporting, '
                        'will be used as a default template for recipients that '
                        'don\'t have a template specified.')
    parser.add_argument('--email', action='store_true',
                        help='Send an email instead of printing out the report.')
    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('-l', '--listen', action='store_true',
                       help='Start polling for rabbitmq messages and create ' +
                       'reports for ready_to_report checkouts.' +
                       ' Defaults to this if no other option was selected.')
    group.add_argument('-c', '--checkout_id', type=str,
                       help='Specify the checkout id of a checkout ' +
                       'you want to create a single report for.')
    return parser


def main():
    """Set up and start consuming messages."""
    misc.sentry_init(sentry_sdk)
    parser = create_parser()
    args = parser.parse_args()

    if args.template is not None:
        settings.SELECTED_TEMPLATE = args.template
    if args.email:
        settings.EMAILING_ENABLED = True

    processing_function = send_emails if settings.EMAILING_ENABLED else print_report

    if args.checkout_id:
        checkout_data = CheckoutData(args.checkout_id)
        processing_function(checkout_data)
        sys.exit(0 if checkout_data.result else 1)  # return 1 on failure
    else:
        # Default to rabbitmq consuming
        print('Now consuming Rabbitmq messages')
        metrics.prometheus_init()
        messagequeue.MessageQueue().consume_messages(
            os.environ.get('WEBHOOK_RECEIVER_EXCHANGE', 'cki.exchange.webhooks'),
            os.environ['REPORTER_ROUTING_KEYS'].split(),
            partial(process_message, func=processing_function),
            queue_name=os.environ.get('REPORTER_QUEUE'),
        )


if __name__ == '__main__':
    main()
